<?php
namespace Cms\Theminator\View\Factory;

use Cms\Theminator\View\Helper\Wizard;
use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

class WizardFactory implements FactoryInterface {
    
    public function createService(ServiceLocatorInterface $serviceLocator) {
        /** @var \Zend\View\HelperPluginManager $serviceLocator */
        $sl = $serviceLocator->getServiceLocator();
        
        return new Wizard($sl->get('ViewManager'));
    }
}