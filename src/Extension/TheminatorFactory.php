<?php
namespace Cms\Theminator\Extension;

use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

class TheminatorFactory implements FactoryInterface {

    public function createService(ServiceLocatorInterface $serviceLocator) {
        return new Theminator();
    }
}