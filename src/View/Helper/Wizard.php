<?php
namespace Cms\Theminator\View\Helper;

use Zend\Json\Expr;
use Zend\Json\Json;
use Zend\View\Model\ViewModel;
use Zend\View\Helper\AbstractHtmlElement;

class Wizard extends AbstractHtmlElement {

    protected $data;

    protected $viewManager;

    public function __construct($viewManager) {
        $this->viewManager = $viewManager;
    }

    public function __invoke(array $data = array()) {
        
        //$this->data = $data;
        
        //$this->injectJs();
        // $widget = new ViewModel($data);
        // if(array_key_exists('template', $data)) {
        //     $widget->setTemplate($data['template']);  
        // }
        

        return $this->getView()->render('installation-wizard', array('foo' => $data));
    }

    public function injectJs($placement = 'APPEND') {
        

        $js = sprintf("$(document).ready(function() { %s });", $this->renderJavascript());
        
        $this->getView()->inlineScript('script', $js, $placement);

        return $this;
    }

    public function renderHtml() {
                
        $html = '<div class="wizard" id="satellite-wizard" data-title="Install CMS">
              
                      <!-- Step 1 -->
                      <div class="wizard-card" data-cardname="name">
                        <h3>General Configuration</h3>

                        <div class="wizard-input-section">
                          <div class="form-group">
                            <div class="col-sm-6">
                              <input type="text" class="form-control" id="sitename" name="sitename" placeholder="Site Name">
                            </div>
                          </div>
                        </div>

                      </div>

                      <!-- Step 2 -->
                      <div class="wizard-card" data-cardname="group">
                        <h3>Admin Access</h3>

                          <div class="wizard-input-section">
                              <p>
                                Admin Email
                              </p>

                              <div class="form-group">
                                <div class="col-sm-8">
                                  <input type="text" class="form-control" id="admin_email" name="admin_email" placeholder="Username" data-serialize="1" />
                                </div>
                              </div>

                              <p>
                                Admin Password
                              </p>

                              <div class="form-group">
                                <div class="col-sm-8">
                                  <input type="text" class="form-control" id="admin_password" name="admin_password" placeholder="Password" data-serialize="1" />
                                </div>
                              </div>

                              <p>
                                Admin Password Again
                              </p>

                              <div class="form-group">
                                <div class="col-sm-8">
                                  <input type="text" class="form-control" id="admin_password_again" name="admin_password_again" placeholder="Password Again" data-serialize="1" />
                                </div>
                              </div>
                            
                          </div>

                      </div>


                      <!-- Step 3 -->
                      <div class="wizard-card" data-cardname="group">
                        <h3>Database Configuration</h3>

                          <div class="wizard-input-section">
                              <p>
                                DB Name
                              </p>

                              <div class="form-group">
                                <div class="col-sm-8">
                                  <input type="text" class="form-control" id="dbname" name="dbname" placeholder="DB Name" data-serialize="1" />
                                </div>
                              </div>

                              <p>
                                DB Selection
                              </p>

                              <div class="form-group">
                                <div class="col-sm-8">
                                      <select class="form-control" id="dbtype" name="dbtype">
                                          <option value="sqlite" selected="selected">Default (Sqlite3)</option>
                                          <option value="couchdb">CouchDB</option>
                                          <option value="mongodb">MongoDB</option>
                                        </select>  
                                </div>
                              </div>

                              <p>
                                DB Password
                              </p>

                              <div class="form-group">
                                <div class="col-sm-8">
                                  <input type="text" class="form-control" id="db_password" name="db_password" placeholder="DB Password" data-serialize="1" />
                                </div>
                              </div>

                          </div>

                          <div class="wizard-error">
                            <div class="alert alert-error">
                              <strong>There was a problem</strong> with your submission.
                              Please correct the errors and re-submit.
                            </div>
                          </div>

                          <div class="wizard-failure">
                            <div class="alert alert-error">
                              <strong>There was a problem</strong> submitting the form.
                              Please try again in a minute.
                            </div>
                          </div>

                          <div class="wizard-success">
                            <div class="alert alert-success">
                              <span class="create-server-name"></span>Installation <strong>Successfully.</strong>
                            </div>

                            <a class="btn btn-success im-done">Done</a>
                          </div>
                      </div>  

                    </div>';
        

        return $html;
    }

    public function renderJavascript() {
        
        $options = '';

        $id = "satellite-wizard";

        $install_key = $this->data['install_key'];
        
        $submitUrl = "/api/$install_key/install.api/start";
        
        return sprintf($this->getJs(), $id, $submitUrl);

    }

    public function getJs() {
      
      return "$.fn.wizard.logging = true;
                
                var wizard = $('#%s').wizard({
                  keyboard : false,
                  contentHeight : 400,
                  contentWidth : 700,
                  backdrop: 'static',
                  submitUrl: '%s'
                });

                wizard.on('submit', function(wizard) {

                    $.ajax({
                        type: 'POST',
                        url: wizard.args.submitUrl,
                        data: wizard.serialize(),
                        dataType: 'json'
                    }).done(function(responder) {
                        
                        if(responder.response.error == true) {
                           wizard.submitError();
                        } else {
                          
                          wizard.submitSuccess();         // displays the success card
                          wizard.hideButtons();           // hides the next and back buttons
                          wizard.updateProgressBar(0);    // sets the progress meter to 0
                        }

                    }).fail(function(response) {
                        console.log(response);
                        wizard.submitError();           // display the error card
                        wizard.hideButtons();           // hides the next and back buttons
                    });
                });
                
                wizard.on('closed', function() {
                  wizard.reset();
                });

                wizard.on('reset', function() {
                  wizard.modal.find(':input').val('').removeAttr('disabled');
                  wizard.modal.find('.form-group').removeClass('has-error').removeClass('has-succes');
                  wizard.modal.find('#fqdn').data('is-valid', 0).data('lookup', 0);
                });

                wizard.el.find('.wizard-success .im-done').click(function() {
                  wizard.hide();
                  setTimeout(function() {
                    wizard.reset(); 
                  }, 250);
                  document.location.href='/admin';
                  
                });
                  
                wizard.el.find('.wizard-success').click(function() {
                  wizard.reset();
                });
                  
                $('#open-wizard').click(function(e) {
                  e.preventDefault();
                  wizard.show();
                });";
    }
}





