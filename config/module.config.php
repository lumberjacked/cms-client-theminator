<?php
return array(

    'service_manager' => include 'service.config.php',

    'view_helpers' => array(
        'factories' => array(
            'wizard'     => 'Cms\Theminator\View\Factory\WizardFactory',
            //'datatables' => 'CmsThemeManager\View\Factory\DatatablesFactory',
        )
    ),

    'view_manager' => array(
        'template_map' => array(
            //'datatables/datatables-template' => __DIR__ . '/../view/datatables/datatables-template.phtml',
            'installation-wizard'            => __DIR__ . '/../view/wizard/installation-wizard.twig'
        )
    ),

    'zfctwig' => array(
        
        'environment_options' => array(
            'debug' => true
        ),

        'extensions' => array(
            'Twig_Extension_Debug'
        ),
    ),



    'bas_cms' => array(
        'extensions' => array(
            'theminator' => array(
                'type'    => 'Cms\Theminator\Extension\Theminator',
                'options' => array(
                    'template_path' => __DIR__ . '/../view' 
                )
            ),
                    
            // 'route-layout' => array(
            //     'type' => 'Cms\Theminator\Adapter\RouteLayout',
            //     'options' => array(
            //         'listeners' => array(
            //             'manager.extension.load' => 'buildLayoutRoutes',
                        
            //         ),
            //         // 'shared-listeners' => array(
            //         //     'Zend\Mvc\Controller\AbstractActionController' =>array(
            //         //         'dispatch' => 'onDispatch'
            //         //     )
            //         // ),
            //         'cms_route_layout' => array(
            //             'enable' => true,
            //             'by_route' => array(
            //                 'cms-login'        => 'layout/cms-login-layout',
            //                 'cms-installation' => 'layout/cms-install-layout'
            //             ),
            //             'by_layout' => array(
            //                 'layout/cms-admin-layout' => array(
            //                      'cms-admin',
            //                      'cms-admin/cms-users',
            //                      'cms-admin/cms-profile',
            //                      'cms-admin/cms-calendar',
            //                      'cms-admin/cms-messages',
            //                      'cms-admin/cms-notifications'
            //                 ),
                             
                            
            //             ),
            //         )     
            //     )
            // ),
            
        ),
    ),
);