<?php

namespace Cms\Theminator\Adapter;

use Zend\Mvc\MvcEvent;
use Zend\EventManager\EventManagerInterface;
use Cms\ExtensionManager\Extension\AbstractExtension;
use Cms\ExtensionManager\Extension\ResponderEvent;

class RouteLayout extends AbstractExtension {

	/**
	 * Simple associative array of routeName=>layoutName
	 * @var array
	 */
	protected $routes = array();

	public function onDispatch(MvcEvent $e) {
        
        $match      = $e->getRouteMatch();
        $controller = $e->getTarget();
        
        $layout = $this->getLayout($match->getMatchedRouteName());
        var_dump($layout);die();
        if(null !== $layout) {
            $controller->layout($layout);
        }
    }

	public function buildLayoutRoutes(ResponderEvent $e) {
		
		$config = $this->getOption('cms_route_layout');
		
		if(isset($config['by_layout']) && is_array($config['by_layout'])) {
			foreach($config['by_layout'] as $layout => $routes) {
				$this->addRoutesByLayout($layout, $routes);
			}
		}


		if(isset($config['by_route']) && is_array($config['by_route'])) {
			foreach($config['by_route'] as $route => $layout) {
				$this->addRoute($route, $layout);
			}
		}
		
		return $e->responder('Layouts successfully built!');

	}
	
	/**
	 * Add multiple routes for the given layout
	 * @param string $layout
	 * @param array $routes
	 * @return self
	 */
	public function addRoutesByLayout($layout, array $routes) {
		foreach($routes as $route) {
			$this->addRoute($route, $layout);
		}
		return $this;
	}
	
	/**
	 * Set the layout for the given route name
	 * @param string $routeName
	 * @param string $layout
	 * @return self
	 * @throws \InvalidArgumentException if either arg is empty
	 */
	public function addRoute($routeName, $layout) {
		if(!is_string($routeName) || empty($routeName)) {
			throw new \InvalidArgumentException("Route must be a non-empty string");
		}
		if(!is_string($layout) ||empty($layout)) {
			throw new \InvalidArgumentException("Layout name must be a non-empty string");
		}
		$this->routes[$routeName] = $layout;
		return $this;
	}

	// public function attachMvcRoute($e) {
	// 	$eventManager = $this->manager->getEventManager();

	// 	$eventManager->attach(MvcEvent::EVENT_ROUTE, array($this, 'getLayout'));
	// }
	
	/**
	 * Return the layout specified for the given route
	 * @param string $routeName
	 * @return string
	 */
	public function getLayout($route_name) {
		
		if(isset($this->routes[$route_name])) {
			return $this->routes[$route_name];
		}
		return null;
	}
	
}