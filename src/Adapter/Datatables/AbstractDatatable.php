<?php
namespace CmsThemeManager\Adapter\Datatables;

use ReflectionClass;
use Zend\Stdlib\Hydrator\ClassMethods;
use Zend\Stdlib\Hydrator\HydratorAwareInterface;
use Zend\Stdlib\Hydrator\HydratorInterface;


abstract class AbstractDatatable implements HydratorAwareInterface {
    
    /**
     * @var String
     */
    protected $id;

    /**
    * @var Array
    */
    protected $scripts_js = array();

    /**
    * @var Array
    **/
    protected $scripts_css = array();

    /**
     * @var DataResult
     */
    protected $dataResult;

    /**
     * @var Collection
     */
    protected $columns;

    protected $hydrator;

    public function injectView($view) {
        
        if(!empty($this->scripts_js)) {
            foreach($this->scripts_js as $index => $js) {
                $view->inlineScript('file', $js, 'APPEND');
            }
        }

        if(!empty($this->scripts_css)) {
            foreach($this->scripts_css as $index => $css) {
                $view->headLink()->appendStylesheet($css);
            }
        }

        return $this;
    }

    public function with($name = null, array $options = array()) {

        if(!empty($options)) {
            $this->setOptions($options);

            if(!isset($options['method'])) {
                $this->setOption('method', 'findAll');
            }

            $method = $this->getOption('method');
        }
        
        if(null !== $name) {
            $this->setColumns(Collection::factory($this->detectColumns($name)));    
        
            $em         = $this->manager->get('Doctrine\ORM\EntityManager');
            $repository = $em->getRepository(get_class($this->manager->get($name)));

            if(method_exists($repository, $method)) {
                $data = $repository->$method();

                $extra_columns = $this->getOption('extra_columns');
                if(null !== $extra_columns) {
                    
                    foreach($data as $index => $object) {
                        
                        foreach($extra_columns as $key => $action) {
                            $object->$key = $action;
                        }
                    }
                }
                
                $this->setDataResult(new DataResult($data, count($data)));
            }
        }

        if(!empty($this->getOption('scripts_js'))) {
            $scripts = $this->getOption('scripts_js');
            $this->addJsFiles($scripts);
        }

        if(!empty($this->getOption('scripts_css'))) {
            $css = $this->getOption('scripts_css');
            $this->addCssFiles($css);
        }
        
        $id = isset($options['id']) ? $options['id'] : $this->sanitizeId($name);
        
        $this->setDatatableId($id);
        
        return $this;
    }

    /**
     * @param string $name
     * @return array
     */
    protected function detectColumns($name) {
        
        $entity       = $this->manager->get($name);
        
        $columns    = array();

        $reflection = new ReflectionClass($entity);
        $properties = $reflection->getProperties();

        foreach($properties as $property) {
            $columns[] = array(
                'sName'  => $property->getName(),
                'sTitle' => ucfirst($property->getName()),
                'mData'  => $property->getName()
            );
        }

        $extra_columns = $this->getOption('extra_columns');
        if(null !== $extra_columns) {
            
            foreach($extra_columns as $title => $action) {
                $columns[] = array(
                    'sName'  => $title,
                    'sTitle' => ucfirst($title),
                    'mData'  => $title    
                );   
            } 
        }
        
        return $columns;
    }

    /**
     * @param string $id
     * @return string
     */
    protected function sanitizeId($id) {
        return strtolower(preg_replace('/[^A-Za-z0-9-]+/', '', $id));
    }

    protected function setDatatableId($id) {
        $this->id = $id;

        return $this;
    }

    public function getDatatableId() {
        return $this->id;
    }

    /**
     * @return bool
     */
    public function isServerSide() {
        return isset($this->options['bServerSide']);
    }

    /**
     * @param AbstractDatatable $datatable
     * @return string
     */
    public function getServerSideBody() {
        $output = str_repeat(' ', 8) . '<tr>';
        $output.= sprintf(
            '<td colspan="%d">Loading data ...</td>',
            count($this->getColumns())
        );
        $output.= '</tr>' . PHP_EOL;

        return $output;
    }

    /**
     * @param AbstractDatatable $datatable
     * @throws \RuntimeException
     * @return string
     */
    public function getStaticBody() {
        $output = '';
        
        foreach($this->getData() as $row) {
            $output.= str_repeat(' ', 8) . '<tr>' . PHP_EOL;

            /** @var $column \CmsThemeManager\Adapter\Datatables\AbstractColumn */
            foreach($this->getColumns() as $column) {
                var_dump($column);
                var_dump($row);
                $style  = ($column->getOption('bVisible') === false) ? ' style="display:none;"' : '';
                $value  = $column->getValue($row);
                
                if (is_object($value)) {
                    $value = '[object]';
                } else if (is_array($value)) {
                    $value = '[array]';
                }

                $output.= sprintf("%s<td%s>%s</td>%s", str_repeat(' ', 12), $style, $value, PHP_EOL);
            }

            $output.= str_repeat(' ', 8) . '</tr>' . PHP_EOL;
        }
        var_dump($output);die('getStaticBody');
        return $output;
    }

    /**
     * Renders the Javascript for the Datatable.
     *
     * @param string|AbstractDatatable $nameOrDatatable
     * @param string|null $id
     * @return string
     */
    public function renderJavascript($nameOrDatatable, $id = null) {
        if (!$id) {
            $id = $this->extractId($nameOrDatatable);
        }
        return sprintf(
            '$("#%s").dataTable(%s);',
            $id,
            $this->renderOptionsJavascript($nameOrDatatable)
        );
    }

    /**
     * Renders only the options portion of the Javascript for Datatables. Useful for setting up
     * javascript instead of using the built in methods. If no custom options are passed in then the
     * options for the datatable are used.
     *
     * @param string|AbstractDatatable $nameOrDatatable
     * @param array|null $options
     * @return string
     */
    public function renderOptionsJavascript($nameOrDatatable, array $options = null) {
        // if (!$nameOrDatatable instanceof AbstractDatatable) {
        //     $nameOrDatatable = $this->manager->get($nameOrDatatable);
        // }

        $options              = $options ? $options : $nameOrDatatable->getOptions();
        $options['aoColumns'] = $nameOrDatatable->getColumns()->toArray();

        foreach($options as $key => $value) {
            if (in_array($key, $this->jsonExpressions)) {
                $input[$key] = new Expr($value);
            }
        }

        // datatables fails with [] instead of {} so cast to object to avoid that
        if (empty($options)) {
            $options = (object) $options;
        }

        $json = Json::encode($options, false, array('enableJsonExprFinder' => true));
        return Json::prettyPrint($json, array('indent' => "    "));
    }

    /**
    *  Adds datatables javascript files to be applied to view later
    *
    * @param array $scripts
    * @return AbstractDatatable
    **/
    protected function addJsFiles(array $scripts = array()) {

        if(!empty($scripts)) {
            $this->scripts_js = $scripts;
        }

        return $this;
    }

    /**
    *  Return scripts array of file paths for datatables
    * @return array $this->scripts
    **/
    public function getJsFiles() {
        return $this->scripts_js;
    }

    /**
    *  Adds datatables javascript files to be applied to view later
    *
    * @param array $scripts
    * @return AbstractDatatable
    **/
    protected function addCssFiles(array $css = array()) {

        if(!empty($css)) {
            $this->scripts_css = $css;
        }

        return $this;
    }

    /**
    *  Return scripts array of file paths for datatables
    * @return array $this->scripts
    **/
    public function getCssFiles() {
        return $this->scripts_css;
    }

    /**
     * @return array
     */
    public function getData() {
        
        if (!$this->dataResult) {
            return array();
        }

        $hydrator = $this->getHydrator();
        
        $result   = array();
        foreach ($this->dataResult->getData() as $data) {
            
            if (is_object($data)) {
                $data = $hydrator->extract($data);
            }
            $result[] = $data;
        }
        
        return $result;
    }

    /**
     * @return DataResult
     */
    public function getDataResult() {
        return $this->dataResult;
    }

    /**
     * @param array|DataResult $dataResult
     * @throws \InvalidArgumentException
     * @return Datatable
     */
    public function setDataResult($dataResult) {
        
        if (is_array($dataResult)) {
            $dataResult = new DataResult($dataResult);
        }

        if (!$dataResult instanceof DataResult) {
            throw new \InvalidArgumentException('DataResult must be an array or instanceof DataResult');
        }
        $this->dataResult = $dataResult;
        return $this;
    }

    /**
     * @param Collection $columns
     * @return Datatable
     */
    public function setColumns(Collection $columns) {
    
        $this->columns = $columns;
        return $this;
    }

    /**
     * @return Collection
     */
    public function getColumns()
    {
        if (!$this->columns instanceof Collection) {
            $this->columns = new Collection();
        }
        return $this->columns;
    }

    /**
     * {@inheritDoc}
     */
    public function setHydrator(HydratorInterface $hydrator) {
        
        $this->hydrator = $hydrator;
        return $this;
    }

    /**
     * {@inheritDoc}
     */
    public function getHydrator() {
        if (!$this->hydrator instanceof HydratorInterface) {
            $this->hydrator = new ClassMethods();
        }
        return $this->hydrator;
    }
}