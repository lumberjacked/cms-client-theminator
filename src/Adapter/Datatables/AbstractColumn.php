<?php

namespace CmsThemeManager\Adapter\Datatables;

/**
 * Class AbstractColumn
 *
 * All options in this class reference http://datatables.net/usage/columns. For options that are new to
 * datatables but not yet handled in this method you can use the setExtraOptions() method. All values
 * default to null which will fallback to the defaults for datatables.
 *
 * @package CmsThemeManager\Adapter\Datatables\Column
 */
abstract class AbstractColumn {

    /**
     * @param array $row
     * @return mixed
     */
    public function getValue(array $row)
    {
        $key = $this->getOption('mDataProp');

        $key = $key ? $key : $this->getOption('mData');
        $key = $key ? $key : $this->getOption('sTitle');
        
        if (!$key) {
            return null;
        }

        return isset($row[$key]) ? $row[$key] : null;
    }

}