<?php
namespace CmsThemeManager\View\Factory;

use CmsThemeManager\View\Helper\Datatables;
use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

class DatatablesFactory implements FactoryInterface {
    
    public function createService(ServiceLocatorInterface $serviceLocator) {
        /** @var \Zend\View\HelperPluginManager $serviceLocator */
        $sl = $serviceLocator->getServiceLocator();
        return new Datatables();
    }
}