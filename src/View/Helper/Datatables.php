<?php
namespace CmsThemeManager\View\Helper;

use Zend\Json\Expr;
use Zend\Json\Json;
use Zend\View\Helper\AbstractHtmlElement;
use CmsThemeManager\Adapter\Datatables\Datatable;

class Datatables extends AbstractHtmlElement {

    public function __invoke(Datatable $datatable) {
      
          $datatable->injectView($this->getView());

          $html = $this->renderHtml($datatable);

          return $this->getView()->render('datatables/datatables-template', array('datatable' => $html));
    }

    /**
     * Renders the HTML for the Datatable.
     *
     * @param string|AbstractDatatable $nameOrDatatable
     * @param string|null $id
     * @param array $attributes
     * @return string
     */
    public function renderHtml($nameOrDatatable) {
      
        $attributes['id'] = $nameOrDatatable->getDatatableId();
        $attributes['class'] = array('table', 'table-hover', 'table-dynamic', 'table-tools');
        
        $columns    = $nameOrDatatable->getColumns();
        $tableStart = sprintf('<table%s>%s', $this->htmlAttribs($attributes), PHP_EOL);
        
        $header     = '';

        if ($columns->count() > 0) {
            $header = str_repeat(' ', 4) . '<thead>' . PHP_EOL;
            $header.= str_repeat(' ', 8) . '<tr>' . PHP_EOL;
            
            /** @var $column \SpiffyDatatables\Column\AbstractColumn */
            foreach($nameOrDatatable->getColumns() as $column) {
                $title  = $column->getOption('sTitle') ? $column->getOption('sTitle') : '';
                $style  = ($column->getOption('bVisible') === false) ? ' style="display:none;"' : '';
                $header.= sprintf("%s<th%s>%s</th>%s", str_repeat(' ', 12), $style, $title, PHP_EOL);
            }

            $header  .= str_repeat(' ', 8) . '</tr>' . PHP_EOL;
            $header  .= str_repeat(' ', 4) . '</thead>' . PHP_EOL;
        }
        
        $body = str_repeat(' ', 4) . '<tbody>' . PHP_EOL;
        
        if ($nameOrDatatable->isServerSide()) {
            $body.= $nameOrDatatable->getServerSideBody();
            
        } else {
          
            $body.= $nameOrDatatable->getStaticBody();
        }

        $body.= str_repeat(' ', 4) . '</tbody>' . PHP_EOL;
        $tableEnd = '</table>';
        
        return $tableStart . $header . $body . $tableEnd;
    }
}





